package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("PersonSummary")
public final class PersonSummary
{
    public PersonSummary() {
    }

    private List<String> featureIds;

    @ThriftField(value=1, name="featureIds", requiredness=Requiredness.OPTIONAL)
    public List<String> getFeatureIds() { return featureIds; }

    @ThriftField
    public void setFeatureIds(final List<String> featureIds) { this.featureIds = featureIds; }

    private int personId;

    @ThriftField(value=2, name="personId", requiredness=Requiredness.REQUIRED)
    public int getPersonId() { return personId; }

    @ThriftField
    public void setPersonId(final int personId) { this.personId = personId; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("featureIds", featureIds)
            .add("personId", personId)
            .toString();
    }
}
