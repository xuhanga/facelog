echo off
rem 生成 IFaceLog erpc 代码脚本
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
pushd "%sh_folder%"
set OUT_FOLDER=%sh_folder%\src\erpc_proxy
:: 指定 erpc compiler(erpcgen) 位置
where erpcgen >nul 2>nul
if %ERRORLEVEL% == 0 (
	set ERPCGEN_EXE=erpcgen
	goto :gen
	) else (
	echo "not found erpcgen.exe,please build erpcgen"
	exit /B -1
	)

:gen
set ERPC_IDL=%sh_folder%\..\facelog-service\IFaceLog.erpc
if NOT exist "%ERPC_IDL%" (
	echo "not found IDL file:%ERPC_IDL%"
	exit /B -1
	)
if exist "%OUT_FOLDER%" (
	del  "%OUT_FOLDER%"\facelog.h* >nul 2>nul
	del  "%OUT_FOLDER%"\facelog_client.* >nul 2>nul
	del  "%OUT_FOLDER%"\facelog_server.* >nul 2>nul
	)
if not exist "%OUT_FOLDER%" mkdir  "%OUT_FOLDER%"

echo ERPC_IDL=%ERPC_IDL%
%ERPCGEN_EXE% -o %OUT_FOLDER% %ERPC_IDL%
popd
