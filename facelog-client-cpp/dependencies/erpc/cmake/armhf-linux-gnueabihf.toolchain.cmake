set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR armhf)

set(_compiler_prefix /usr)
if($ENV{CROSS_COMPILER_PREFIX})
    set(_compiler_prefix $ENV{CROSS_COMPILER_PREFIX})
elseif(CROSS_COMPILER_PREFIX)
    set(_compiler_prefix ${CROSS_COMPILER_PREFIX})
endif()
set(CMAKE_C_COMPILER ${_compiler_prefix}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${_compiler_prefix}/bin/arm-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

unset(_compiler_prefix)