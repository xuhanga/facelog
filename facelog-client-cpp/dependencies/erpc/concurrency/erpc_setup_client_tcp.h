/*
 * Copyright (c) 2020, ZYtech, Inc.
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _ERPC_SETUP_CLIENT_TCP_H_
#define _ERPC_SETUP_CLIENT_TCP_H_

#include "erpc_transport_setup.h"
/*!
 * @{
 * @brief erpc::TCPTransport类的标准C封装
 * @file
 */


////////////////////////////////////////////////////////////////////////////////
// API
////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//! @name TCP/IP transport
//@{

/*!
 * @brief Create an TCP/IP transport.
 *
 * @param[in] host Specify the host name or IP address of the computer.
 * @param[in] port Specify the listening port number.
 * @return Return NULL or erpc_transport_t instance pointer.
 */
erpc_transport_t erpc_transport_client_tcp_init(const char *host, uint16_t port);
/*!
 * @brief Deinitialize an TCP/IP transport.
 *
 * This function deinitializes the TCP/IP transport.
 */
void erpc_transport_client_tcp_deinit(void);

//@}

#ifdef __cplusplus
}
#endif

/*! @} */

#endif // _ERPC_SETUP_CLIENT_TCP_H_
