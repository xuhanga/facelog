#cmake file for thrift wrapper
#author:guyadong
#created:2020/03/21
# 查找 thrift库
find_package(Thrift REQUIRED)
set (INTFACE_NAME facelogclient)
# 定义所有的源文件列表
aux_source_directory(${CMAKE_CURRENT_LIST_DIR}/stub _STUB_SOURCES)
file(GLOB _STUB_HEADERS RELATIVE ${CMAKE_CURRENT_LIST_DIR} "${CMAKE_CURRENT_LIST_DIR}/stub/*.h")
aux_source_directory(${CMAKE_CURRENT_LIST_DIR} _WRAPPER_SOURCES)
file(GLOB _WRAPPER_HEADERS RELATIVE ${CMAKE_CURRENT_LIST_DIR} "${CMAKE_CURRENT_LIST_DIR}/*.h")

set(_client_targets)
if(MSVC) 
	if(TARGET thrift_static_mt)
		message("-- create target: ${INTFACE_NAME}_mt")
		add_library(${INTFACE_NAME}_mt STATIC ${_STUB_SOURCES} ${_WRAPPER_SOURCES})
		with_mt_if_msvc(${INTFACE_NAME}_mt)
		list(APPEND _client_targets ${INTFACE_NAME}_mt)
	endif(TARGET thrift_static_mt)
	if(TARGET thrift_static_md)
		message("-- create target: ${INTFACE_NAME}_md")
		add_library(${INTFACE_NAME}_md STATIC ${_STUB_SOURCES} ${_WRAPPER_SOURCES})
		list(APPEND _client_targets ${INTFACE_NAME}_md )
	endif(TARGET thrift_static_md)
else()
	if(TARGET thrift_static)
		message("-- create target: ${INTFACE_NAME}")
		add_library(${INTFACE_NAME} STATIC ${_STUB_SOURCES} ${_WRAPPER_SOURCES})
		list(APPEND _client_targets ${INTFACE_NAME} )
	endif(TARGET thrift_static)
endif(MSVC)

if(NOT _client_targets)
	message("NOT TARGET DEFINED")
	return()
endif()

set(_public_headers)
list(APPEND _public_headers ${_WRAPPER_HEADERS} )

#########################
# 设置项目属性
function (define_client_target target)
if(NOT TARGET ${target})
	return ()
endif()
# 根据target名字后缀计算 thrift_static target后缀名
string(REGEX MATCH "_m[td]$" _static_postfix "${target}")
# MSVC Debug 编译时生成包含用于调试器的完整符号调试信息的 .obj 文件
# see also https://msdn.microsoft.com/zh-cn/library/958x11bc.aspx
# 添加NOMINMAX 避免与std::min(),std::max()函数宏定义min,max冲突
target_compile_options(${target} PRIVATE $<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Debug>>:/Z7> 
	$<$<CXX_COMPILER_ID:MSVC>:/wd4275 /wd4251>
	$<$<CXX_COMPILER_ID:GNU>:-Wno-format-security>)
target_compile_definitions(${target} 
					PRIVATE $<$<CXX_COMPILER_ID:MSVC>:NOMINMAX> 
	        		PUBLIC _SL_USE_BYTE_STREAM 
					)
get_target_property(_type ${target} TYPE)
target_include_directories (${target}
	INTERFACE "$<INSTALL_INTERFACE:include>"
			  "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}>"
			  "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/stub>"
	PRIVATE "${CMAKE_CURRENT_LIST_DIR}/stub"
	)
set_target_properties (${target}  PROPERTIES 
	PUBLIC_HEADER "${_public_headers}"
	VERSION ${PROJECT_VERSION} 
	SOVERSION ${PROJECT_VERSION_MAJOR}
	FOLDER "facelog"
	)
set_target_properties (${target}  PROPERTIES 
	DEBUG_POSTFIX _d
	)

set(_link_libraries thrift_static${_static_postfix})
if(CMAKE_SYSTEM_NAME MATCHES "Linux")
	list(APPEND _link_libraries -pthread)
endif()

target_link_libraries(${target} PUBLIC ${_link_libraries})
set_target_properties (${target}  PROPERTIES INTERFACE_POSITION_INDEPENDENT_CODE ON
														POSITION_INDEPENDENT_CODE ON)
target_compile_definitions(${target} PUBLIC IFACELOG_STATIC)
target_link_libraries(${target} PRIVATE $<BUILD_INTERFACE:common_source> ) 

endfunction(define_client_target)
#####################

foreach( _target ${_client_targets} )
	define_client_target(${_target})
endforeach()

# ----------------------------------------------------------------------------
include (CMakePackageConfigHelpers)
set (facelog_client_DEPENDENCY "find_dependency(Thrift)")
configure_package_config_file (${PROJECT_SOURCE_DIR}/cmake/config.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/${INTFACE_NAME}-config.cmake
  INSTALL_DESTINATION ${CONFIG_INSTALL_DIR}
  NO_CHECK_REQUIRED_COMPONENTS_MACRO)
write_basic_package_version_file (${INTFACE_NAME}-config-version.cmake VERSION
  ${PROJECT_VERSION} COMPATIBILITY SameMajorVersion)
################安装脚本#################
install(TARGETS ${_client_targets} EXPORT ${INTFACE_NAME}-targets
  RUNTIME DESTINATION ${RUNTIME_INSTALL_DIR}
  LIBRARY DESTINATION ${LIBRARY_INSTALL_DIR}
  ARCHIVE DESTINATION ${LIBRARY_INSTALL_DIR}
  PUBLIC_HEADER DESTINATION ${INCLUDE_INSTALL_DIR}/${INTFACE_NAME}
  )
install (FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${INTFACE_NAME}-config.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/${INTFACE_NAME}-config-version.cmake
  DESTINATION ${CONFIG_INSTALL_DIR}
  )

install (EXPORT ${INTFACE_NAME}-targets NAMESPACE gdface:: DESTINATION ${CONFIG_INSTALL_DIR} )

unset(_STUB_SOURCES)
unset(_WRAPPER_SOURCES)
unset(_STUB_HEADERS)
unset(_WRAPPER_HEADERS)
unset(_public_headers)