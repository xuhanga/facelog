/*
* connect_test.cpp
* client端连接测试
*  Created on: 2020年03月17日
*      Author: guyadong
*/
#include <iostream>
#include "IFaceLogIfClient.h"
#include "sample_log.h"
#include "BaseCmdParam.h"
#include <string>
#include <algorithm>
using namespace gdface;
using namespace net::gdface::facelog;
using namespace codegen;
// 调用 versionInfo()接口方法
void show_version_info(IFaceLogIfClient &client) {	
	auto r = client.versionInfo();
	std::map<std::string, std::string> version_info = *r;
	for (auto entry : version_info) {
		std::cout << "key=" << entry.first << " value=" << entry.second << std::endl;
	}
}
int main(int argc, char *argv[]) {
	BaseClientConfig param;
	param.parse(argc, argv);
	auto client = IFaceLogIfClient(param.host, param.port);
	try {
		SAMPLE_OUT("connect service:{}@{} ...", param.port, param.host);
		// 调用 isLocal()接口方法
		SAMPLE_OUT("isLocal respone:{}", client.isLocal());
		show_version_info(client);
	}
	catch (std::exception& tx) {
		SAMPLE_ERR("ERROR: {}", tx.what());
	}
}
