#!/bin/bash
# multicast test
method=test
[ -n "$1" ] && method=$1
mvn -Dtest=MultiCastTest#$method -DskipTests=false test