package net.gdface.service.facelog;

import java.net.SocketAddress;

import com.facebook.nifty.core.ConnectionContext;
import com.facebook.nifty.core.RequestContext;
import com.facebook.nifty.core.RequestContexts;

import net.gdface.facelog.FacelogDecorator.BaseClientAddressProbe;

/**
 * 用于Nifty(thrift)的客户端地址侦听器
 * @author guyadong
 *
 */
class ClientAddressProbeForNifty extends BaseClientAddressProbe {
	static ClientAddressProbeForNifty NIFTY_ADDRESS_PROBE = new ClientAddressProbeForNifty(); 
	ClientAddressProbeForNifty() {
	}
	/**
	 * 返回客户端IP地址
	 * @return IP地址对象
	 */
	private static final SocketAddress niftyClientAddress(){
		RequestContext request = RequestContexts.getCurrentContext();
		if(null == request){
			return null;
		}
		ConnectionContext connect = request.getConnectionContext();
		return connect.getRemoteAddress();	
	}
	public static final String niftyClientAddressAsString(){
		SocketAddress address = niftyClientAddress();
		return null == address ? null :address.toString();
	}
	@Override
	protected String getClientAddress() {
		return niftyClientAddressAsString();
	}

}
