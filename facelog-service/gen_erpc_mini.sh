#!/bin/bash
# 生成 facelog eRPC 剪裁接口定义文件(IDL)
sh_folder=$(dirname $(readlink -f $0))
pushd "$sh_folder"
mvn com.gitee.l0km:swift2thrift-maven-plugin:generate@erpc_mini %*
popd
