package net.gdface.facelog;

import java.util.List;

/**
 * 人员(摘要)信息<br>
 * 包含对应人员的特征码ID(MD5),具体所属SDK版本号由使用者决定
 * @author guyadong
 *
 */
public class PersonSummary{
	/** 人员ID */
	private int personId;
	/**
	 * 人员ID对应的特征码ID(MD5)列表
	 */
	private List<String> featureIds;
	public PersonSummary() {
		super();
	}
	public PersonSummary(int personId, List<String> featureIds) {
		super();
		this.personId = personId;
		this.featureIds = featureIds;
	}
	/**
	 * @return personId
	 */
	public int getPersonId() {
		return personId;
	}
	/**
	 * @param personId 要设置的 personId
	 */
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	/**
	 * @return featureIds
	 */
	public List<String> getFeatureIds() {
		return featureIds;
	}
	/**
	 * @param featureIds 要设置的 featureIds
	 */
	public void setFeatureIds(List<String> featureIds) {
		this.featureIds = featureIds;
	}
}