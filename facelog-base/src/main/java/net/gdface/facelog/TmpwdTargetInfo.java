package net.gdface.facelog;

import java.util.Date;

import net.gdface.annotation.CodegenRequired;

/**
 * 临时密码作用目标描述对象<br>
 * 临时密码用于为设备提供临时的访问权限
 * @author guyadong
 *
 */
public class TmpwdTargetInfo implements CommonConstant{
	/**
	 * 定义 {@link #targetId}的类型
	 */
	@CodegenRequired
	private TmpPwdTargetType targetType;
	/**
	 * 临时密码作用目标,类型由{@link #targetType}定义
	 */
	private int targetId;
	/**
	 * 临时密码的到期时间
	 */
	@CodegenRequired
	private Date expiryDate;
	public TmpwdTargetInfo() {
	}
	
	public TmpwdTargetInfo(TmpPwdTargetType targetType, int targetId, Date expiryDate) {
		super();
		this.targetType = targetType;
		this.targetId = targetId;
		this.expiryDate = expiryDate;
	}

	public TmpPwdTargetType getTargetType() {
		return targetType;
	}
	public void setTargetType(TmpPwdTargetType targetType) {
		this.targetType = targetType;
	}

	public int getTargetId() {
		return targetId;
	}
	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}
	public Date getExpiryDate(){
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + targetId;
		result = prime * result + ((targetType == null) ? 0 : targetType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TmpwdTargetInfo))
			return false;
		TmpwdTargetInfo other = (TmpwdTargetInfo) obj;
		if (targetId != other.targetId)
			return false;
		if (targetType != other.targetType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TmpwdTargetInfo [targetType=");
		builder.append(targetType);
		builder.append(", targetId=");
		builder.append(targetId);
		builder.append("]");
		return builder.toString();
	}

}
