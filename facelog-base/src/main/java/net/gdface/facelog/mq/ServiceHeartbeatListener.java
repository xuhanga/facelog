package net.gdface.facelog.mq;

import gu.simplemq.IMessageAdapter;

/**
 * 服务心跳侦听器
 * @author guyadong
 *
 */
public interface ServiceHeartbeatListener extends IMessageAdapter<ServiceHeartbeatPackage>{ 
}