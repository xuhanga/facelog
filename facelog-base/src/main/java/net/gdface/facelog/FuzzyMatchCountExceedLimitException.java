package net.gdface.facelog;

/**
 * 模糊匹配结果数量超过指定的限制异常
 * @author guyadong
 *
 */
public class FuzzyMatchCountExceedLimitException extends Exception {

	private static final long serialVersionUID = 1L;

	public FuzzyMatchCountExceedLimitException() {
		super();
	}

}
