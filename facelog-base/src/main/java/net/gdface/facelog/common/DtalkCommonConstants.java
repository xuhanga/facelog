package net.gdface.facelog.common;

public interface DtalkCommonConstants {

	/** 算法授权 */
	String CMD_LICENSE = "license";
	/** 调用faceapi接口 */
	String CMD_FACEAPI = "faceapi";
	/** 调用faceapi接口参数：sdk版本号 */
	String CMD_FACEAPI_SDKVERSION = "sdkVersion";
	/** 调用faceapi接口参数：接口方法名 */
	String CMD_FACEAPI_METHOD = "method";
	/** 调用faceapi接口参数：接口方法参数 */
	String CMD_FACEAPI_PARAMETERS = "parameters";
	/** 通用提取人脸特征命令 */
	String CMD_EXTRACT = "extractFeature";
	/** 通用提取人脸特征命令参数:图像数据 */
	String CMD_EXTRACT_IMAGE = "image";
	/** 基本命令所在菜单名 */
	String MENU_CMD = "cmd";
	/* 扩展命令所在菜单名 */
	String MENU_CMD_EXT = "cmdext";

}
