package net.gdface.facelog;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.base.Strings;

import gu.simplemq.Channel;
import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import net.gdface.facelog.db.LogBean;
import net.gdface.facelog.CommonConstant;

/**
 * 验证日志表({@code fl_log})增加侦听器<br>
 * 当{@code fl_log}记录增加时发布订阅消息
 * @author guyadong
 *
 */
class MQLogListener extends TableListener.Adapter<LogBean> implements CommonConstant{

	private final IPublisher publisher;
	private final Channel<LogBean> channel;
	
	/**
	 * 使用{@link IMessageQueueFactory}默认实例
	 * @param logMonitorChannel
	 */
	public MQLogListener(String logMonitorChannel) {
		this(logMonitorChannel,MessageQueueFactorys.getDefaultFactory());
	}
	/**
	 * 构造方法
	 * @param logMonitorChannel 人员验证实时监控通道名
	 * @param factory
	 */
	public MQLogListener(String logMonitorChannel,IMessageQueueFactory factory) {
		checkArgument(!Strings.isNullOrEmpty(logMonitorChannel),"INVALID logMonitorChannel %s",logMonitorChannel);
		this.channel = new Channel<LogBean>(logMonitorChannel){};
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(LogBean bean) {
		new PublishTask<LogBean>(
				channel, 
				bean, 
				publisher)
		.execute();		
	}
}
