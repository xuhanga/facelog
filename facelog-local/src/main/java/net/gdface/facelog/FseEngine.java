package net.gdface.facelog;

import static com.google.common.base.Preconditions.*;

import net.gdface.facelog.db.FeatureBean;
import net.gdface.sdk.fse.BaseFseDbEngine;
import net.gdface.sdk.fse.CodeBean;
import net.gdface.sdk.fse.FeatureSe;
import net.gdface.utils.BinaryUtils;

/**
 * 继承 {@link BaseFseDbEngine}类
 * 将 fl_feature 表中特定SDK版本号的特征记录加入 FSE 引擎,实现内存特征搜索
 * @author guyadong
 *
 */
public class FseEngine extends BaseFseDbEngine<FeatureBean> {

	private final String sdkVersion;
	
	/**
	 * 构造方法
	 * @param fse FSE引擎实例
	 * @param sdkVersion SDK版本号
	 */
	public FseEngine(FeatureSe fse,String sdkVersion) {
		super(fse);
		this.sdkVersion = checkNotNull(sdkVersion,"sdkVersion is null");
	}

	@Override
	protected boolean beanFilter(FeatureBean bean) {
		return bean == null ? false : sdkVersion.equals(bean.getVersion());
	}

	@Override
	protected byte[] featureOf(FeatureBean bean) {
		return null == bean ? null : BinaryUtils.getBytesInBuffer(bean.getFeature());
	}

	@Override
	protected byte[] featureIdOf(FeatureBean bean) {
		return null == bean ? null : BinaryUtils.hex2Bytes(bean.getMd5());
	}

	@Override
	protected String ownerOf(FeatureBean bean) {
		if(null == bean){
			return null;
		}
		return bean.getPersonId() == null ? null : CodeBean.asImgMD5(bean.getPersonId()) ;
	}

}
