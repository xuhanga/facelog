# 说明

本项目的数据库访问代码(ORM)由[sqlj2ava](https://gitee.com/l0km/sql2java)工具生成。

# 建表语句
sql/create_table.sql MySQL数据库建表脚本

gen-sql.xml Maven脚本执行 create_table.sql

使用示例如下(需要先启动msqyl数据库):

	mvn -f gen-sql.xml sql:execute -Ddb.url=jdbc:mysql://localhost:3306/test?characterEncoding=utf8
	# 如果不指定 db.url参数，则使用默认本地数据库实例(参见gen-sql.xml中db.url定义)

# 生成代码

> 运行要求： JDK 1.7 or 1.8, Maven 3.5.0 or above,mysql 5.6(or above) running

- gen-mysql.sh gen-mysql.bat 用于生成接口层代码的简单脚本
- gen-mysql.properties sql2java生成接口层代码的配置文件,执行脚本前务必确认文件中jdbc.drive,jdbc.url,jdbc.username,jdbc.password,jdbc.schema等参数正确配置



使用示例如下:

	$ gen-mysql.bat
	[INFO] Scanning for projects...
	[INFO] ------------------------------------------------------------------------
	[INFO] Reactor Build Order:
	[INFO] 
	[INFO] facelog-db2
	[INFO] facelog-db2-base
	[INFO] 
	[INFO] ------------------------------------------------------------------------
	[INFO] Building facelog-db2 2.5.2-SNAPSHOT
	[INFO] ------------------------------------------------------------------------
	[INFO] 
	[INFO] --- sql2java-maven-plugin:1.0.6-SNAPSHOT:generate (default-cli) @ facelog-db2 ---
	        database properties initialization
	[INFO] classpath: [file:/J:/facelog/db2/lib/mysql-connector-java-5.1.43-bin.jar]
	Connecting to root on jdbc:mysql://localhost:3306/test?characterEncoding=utf8&useInformationSchema=true ...
	    Connected.
	    Database server :MySQL.
	Loading table list according to pattern fl_% ...
	    table fl_device found
	    table fl_device_group found
	    table fl_face found
	    table fl_feature found
	    table fl_image found
	    table fl_log found
	    table fl_permit found
	    table fl_person found
	    table fl_person_group found
	    table fl_store found
	    table fl_log_light found
	samePrefix = [fl_]
	Loading columns ...
	        fl_device.id INT AUTOINCREMENT default value: null
	        fl_device.group_id INT  default value: 1
	        fl_device.name VARCHAR  default value: null
	        fl_device.product_name VARCHAR  default value: null
	        fl_device.model VARCHAR  default value: null
	        fl_device.vendor VARCHAR  default value: null
	        fl_device.manufacturer VARCHAR  default value: null
	        fl_device.made_date DATE  default value: null
	        fl_device.version VARCHAR  default value: null
	        fl_device.used_sdks VARCHAR  default value: null
	        fl_device.serial_no VARCHAR  default value: null
	        fl_device.mac CHAR  default value: null
	        fl_device.direction INT  default value: null
	        fl_device.remark VARCHAR  default value: null
	        fl_device.ext_bin BLOB  default value: null
	        fl_device.ext_txt TEXT  default value: null
	        fl_device.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	        fl_device.update_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_device found 18 columns
	        fl_device_group.id INT AUTOINCREMENT default value: null
	        fl_device_group.name VARCHAR  default value: null
	        fl_device_group.leaf TINYINT  default value: null
	        fl_device_group.parent INT  default value: null
	        fl_device_group.root_group INT  default value: null
	        fl_device_group.schedule VARCHAR  default value: null
	        fl_device_group.remark VARCHAR  default value: null
	        fl_device_group.ext_bin BLOB  default value: null
	        fl_device_group.ext_txt TEXT  default value: null
	        fl_device_group.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	        fl_device_group.update_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_device_group found 11 columns
	        fl_face.id INT AUTOINCREMENT default value: null
	        fl_face.image_md5 CHAR  default value: null
	        fl_face.face_left INT  default value: null
	        fl_face.face_top INT  default value: null
	        fl_face.face_width INT  default value: null
	        fl_face.face_height INT  default value: null
	        fl_face.eye_leftx INT  default value: null
	        fl_face.eye_lefty INT  default value: null
	        fl_face.eye_rightx INT  default value: null
	        fl_face.eye_righty INT  default value: null
	        fl_face.mouth_x INT  default value: null
	        fl_face.mouth_y INT  default value: null
	        fl_face.nose_x INT  default value: null
	        fl_face.nose_y INT  default value: null
	        fl_face.angle_yaw INT  default value: null
	        fl_face.angle_pitch INT  default value: null
	        fl_face.angle_roll INT  default value: null
	        fl_face.ext_info BLOB  default value: null
	        fl_face.feature_md5 CHAR  default value: null
	    fl_face found 19 columns
	        fl_feature.md5 CHAR  default value: null
	        fl_feature.version VARCHAR  default value: null
	        fl_feature.person_id INT  default value: null
	        fl_feature.feature BLOB  default value: null
	        fl_feature.update_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_feature found 5 columns
	        fl_image.md5 CHAR  default value: null
	        fl_image.format VARCHAR  default value: null
	        fl_image.width INT  default value: null
	        fl_image.height INT  default value: null
	        fl_image.depth INT  default value: 0
	        fl_image.face_num INT  default value: 0
	        fl_image.thumb_md5 CHAR  default value: null
	        fl_image.device_id INT  default value: null
	    fl_image found 8 columns
	        fl_log.id INT AUTOINCREMENT default value: null
	        fl_log.person_id INT  default value: null
	        fl_log.device_id INT  default value: null
	        fl_log.verify_feature CHAR  default value: null
	        fl_log.compare_face INT  default value: null
	        fl_log.verify_status TINYINT  default value: null
	        fl_log.similarty DOUBLE  default value: null
	        fl_log.direction INT  default value: null
	        fl_log.verify_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	        fl_log.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_log found 10 columns
	        fl_permit.device_group_id INT  default value: null
	        fl_permit.person_group_id INT  default value: null
	        fl_permit.schedule VARCHAR  default value: null
	        fl_permit.pass_limit VARCHAR  default value: null
	        fl_permit.remark VARCHAR  default value: null
	        fl_permit.ext_bin BLOB  default value: null
	        fl_permit.ext_txt TEXT  default value: null
	        fl_permit.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_permit found 8 columns
	        fl_person.id INT AUTOINCREMENT default value: null
	        fl_person.group_id INT  default value: 1
	        fl_person.name VARCHAR  default value: null
	        fl_person.sex TINYINT  default value: null
	        fl_person.rank TINYINT  default value: null
	        fl_person.password CHAR  default value: null
	        fl_person.birthdate DATE  default value: null
	        fl_person.mobile_phone CHAR  default value: null
	        fl_person.papers_type TINYINT  default value: null
	        fl_person.papers_num VARCHAR  default value: null
	        fl_person.image_md5 CHAR  default value: null
	        fl_person.expiry_date DATE  default value: 2050-12-31
	        fl_person.activated_date DATE  default value: null
	        fl_person.remark VARCHAR  default value: null
	        fl_person.ext_bin BLOB  default value: null
	        fl_person.ext_txt TEXT  default value: null
	        fl_person.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	        fl_person.update_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_person found 18 columns
	        fl_person_group.id INT AUTOINCREMENT default value: null
	        fl_person_group.name VARCHAR  default value: null
	        fl_person_group.leaf TINYINT  default value: null
	        fl_person_group.parent INT  default value: null
	        fl_person_group.root_group INT  default value: null
	        fl_person_group.remark VARCHAR  default value: null
	        fl_person_group.ext_bin BLOB  default value: null
	        fl_person_group.ext_txt TEXT  default value: null
	        fl_person_group.create_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	        fl_person_group.update_time TIMESTAMP  default value: CURRENT_TIMESTAMP
	    fl_person_group found 10 columns
	        fl_store.md5 CHAR  default value: null
	        fl_store.encoding VARCHAR  default value: null
	        fl_store.data MEDIUMBLOB  default value: null
	    fl_store found 3 columns
	        fl_log_light.id INT  default value: 0
	        fl_log_light.person_id INT  default value: 0
	        fl_log_light.name VARCHAR  default value: null
	        fl_log_light.papers_type TINYINT  default value: null
	        fl_log_light.papers_num VARCHAR  default value: null
	        fl_log_light.verify_time TIMESTAMP  default value: 0000-00-00 00:00:00
	        fl_log_light.direction INT  default value: null
	    fl_log_light found 7 columns
	Database::loadPrimaryKeys
	Found primary key (seq,name) (1,id) for table 'fl_device'
	Found primary key (seq,name) (1,id) for table 'fl_device_group'
	Found primary key (seq,name) (1,id) for table 'fl_face'
	Found primary key (seq,name) (1,md5) for table 'fl_feature'
	Found primary key (seq,name) (1,md5) for table 'fl_image'
	Found primary key (seq,name) (1,id) for table 'fl_log'
	Found primary key (seq,name) (1,device_group_id) for table 'fl_permit'
	Found primary key (seq,name) (2,person_group_id) for table 'fl_permit'
	Found primary key (seq,name) (1,id) for table 'fl_person'
	Found primary key (seq,name) (1,id) for table 'fl_person_group'
	Found primary key (seq,name) (1,md5) for table 'fl_store'
	Loading imported keys ...
	    fl_device.group_id -> fl_device_group.id found seq:1 foreign key name:fl_device_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_device_group.parent -> fl_device_group.id found seq:1 foreign key name:fl_device_group_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_face.feature_md5 -> fl_feature.md5 found seq:1 foreign key name:fl_face_ibfk_2
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_face.image_md5 -> fl_image.md5 found seq:1 foreign key name:fl_face_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:CASCADE
	    fl_feature.person_id -> fl_person.id found seq:1 foreign key name:fl_feature_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:CASCADE
	    fl_image.device_id -> fl_device.id found seq:1 foreign key name:fl_image_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_log.device_id -> fl_device.id found seq:1 foreign key name:fl_log_ibfk_2
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_log.compare_face -> fl_face.id found seq:1 foreign key name:fl_log_ibfk_4
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_log.verify_feature -> fl_feature.md5 found seq:1 foreign key name:fl_log_ibfk_3
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_log.person_id -> fl_person.id found seq:1 foreign key name:fl_log_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:CASCADE
	    fl_permit.device_group_id -> fl_device_group.id found seq:1 foreign key name:fl_permit_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:CASCADE
	    fl_permit.person_group_id -> fl_person_group.id found seq:1 foreign key name:fl_permit_ibfk_2
	    UPDATE_RULE:RESTRICT    DELETE_RULE:CASCADE
	    fl_person.image_md5 -> fl_image.md5 found seq:1 foreign key name:fl_person_ibfk_2
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_person.group_id -> fl_person_group.id found seq:1 foreign key name:fl_person_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	    fl_person_group.parent -> fl_person_group.id found seq:1 foreign key name:fl_person_group_ibfk_1
	    UPDATE_RULE:RESTRICT    DELETE_RULE:SET_NULL
	Loading indexes ...
	  Found interesting index mac on mac for table fl_device
	  Found interesting index serial_no on serial_no for table fl_device
	  Found interesting index group_id on group_id for table fl_device
	  Found interesting index parent on parent for table fl_device_group
	  Found interesting index feature_md5 on feature_md5 for table fl_face
	  Found interesting index image_md5 on image_md5 for table fl_face
	  Found interesting index feature_version on version for table fl_feature
	  Found interesting index person_id on person_id for table fl_feature
	  Found interesting index device_id on device_id for table fl_image
	  Found interesting index compare_face on compare_face for table fl_log
	  Found interesting index device_id on device_id for table fl_log
	  Found interesting index person_id on person_id for table fl_log
	  Found interesting index verify_feature on verify_feature for table fl_log
	  Found interesting index image_md5 on image_md5 for table fl_person
	  Found interesting index mobile_phone on mobile_phone for table fl_person
	  Found interesting index papers_num on papers_num for table fl_person
	  Found interesting index expiry_date on expiry_date for table fl_person
	  Found interesting index group_id on group_id for table fl_person
	  Found interesting index parent on parent for table fl_person_group
	Loading procedures ...
	Generating template /templates/velocity/java5g/perschema/constant.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\Constant.java
	    java\net\gdface\facelog\db\Constant.java done.
	Generating template /templates/velocity/java5g/perschema/database.properties.vm
	 .... writing to facelog-db-base/src/main\resources/conf\database.properties
	    resources/conf\database.properties done.
	Generating template /templates/velocity/java5g/perschema/gu.sql2java.irowmetadata.vm
	 .... writing to facelog-db-base/src/main\resources/META-INF/services\gu.sql2java.IRowMetaData
	    resources/META-INF/services\gu.sql2java.IRowMetaData done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\DeviceBean.java
	    java\net\gdface\facelog\db\DeviceBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IDeviceManager.java
	    java\net\gdface\facelog\db\IDeviceManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\DeviceMetaData.java
	    java\net\gdface\facelog\db\DeviceMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\DeviceGroupBean.java
	    java\net\gdface\facelog\db\DeviceGroupBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IDeviceGroupManager.java
	    java\net\gdface\facelog\db\IDeviceGroupManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\DeviceGroupMetaData.java
	    java\net\gdface\facelog\db\DeviceGroupMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\FaceBean.java
	    java\net\gdface\facelog\db\FaceBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IFaceManager.java
	    java\net\gdface\facelog\db\IFaceManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\FaceMetaData.java
	    java\net\gdface\facelog\db\FaceMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\FeatureBean.java
	    java\net\gdface\facelog\db\FeatureBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IFeatureManager.java
	    java\net\gdface\facelog\db\IFeatureManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\FeatureMetaData.java
	    java\net\gdface\facelog\db\FeatureMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\ImageBean.java
	    java\net\gdface\facelog\db\ImageBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IImageManager.java
	    java\net\gdface\facelog\db\IImageManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\ImageMetaData.java
	    java\net\gdface\facelog\db\ImageMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\LogBean.java
	    java\net\gdface\facelog\db\LogBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\ILogManager.java
	    java\net\gdface\facelog\db\ILogManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\LogMetaData.java
	    java\net\gdface\facelog\db\LogMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PermitBean.java
	    java\net\gdface\facelog\db\PermitBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IPermitManager.java
	    java\net\gdface\facelog\db\IPermitManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PermitMetaData.java
	    java\net\gdface\facelog\db\PermitMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PersonBean.java
	    java\net\gdface\facelog\db\PersonBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IPersonManager.java
	    java\net\gdface\facelog\db\IPersonManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PersonMetaData.java
	    java\net\gdface\facelog\db\PersonMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PersonGroupBean.java
	    java\net\gdface\facelog\db\PersonGroupBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IPersonGroupManager.java
	    java\net\gdface\facelog\db\IPersonGroupManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\PersonGroupMetaData.java
	    java\net\gdface\facelog\db\PersonGroupMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\StoreBean.java
	    java\net\gdface\facelog\db\StoreBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\IStoreManager.java
	    java\net\gdface\facelog\db\IStoreManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\StoreMetaData.java
	    java\net\gdface\facelog\db\StoreMetaData.java done.
	Generating template /templates/velocity/java5g/pertable/bean.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\LogLightBean.java
	    java\net\gdface\facelog\db\LogLightBean.java done.
	Generating template /templates/velocity/java5g/pertable/manager.interface.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\ILogLightManager.java
	    java\net\gdface\facelog\db\ILogLightManager.java done.
	Generating template /templates/velocity/java5g/pertable/metadata.java.vm
	 .... writing to facelog-db-base/src/main\java\net\gdface\facelog\db\LogLightMetaData.java
	    java\net\gdface\facelog\db\LogLightMetaData.java done.
	Generating template vm/custom/perschema/channel.constant.java.vm
	 .... writing to ../facelog-local/src/sql2java\java\net\gdface\facelog\ChannelConstant.java
	    java\net\gdface\facelog\ChannelConstant.java done.
	Generating template vm/custom/perschema/sub.adapters.java.vm
	 .... writing to ../facelog-local/src/sql2java\java\net\gdface\facelog\SubAdapters.java
	    java\net\gdface\facelog\SubAdapters.java done.
	Generating template ../facelog-local/vm/custom/perschema/base.dao.java.vm
	 .... writing to ../facelog-local/src/sql2java\java\net\gdface\facelog\BaseDao.java
	    java\net\gdface\facelog\BaseDao.java done.
	[INFO] ------------------------------------------------------------------------
	[INFO] Reactor Summary:
	[INFO] 
	[INFO] facelog-db2 ........................................ SUCCESS [  3.723 s]
	[INFO] facelog-db2-base ................................... SKIPPED
	[INFO] ------------------------------------------------------------------------
	[INFO] BUILD SUCCESS
	[INFO] ------------------------------------------------------------------------
	[INFO] Total time: 3.882 s
	[INFO] Finished at: 2019-12-14T22:25:50+08:00
	[INFO] Final Memory: 14M/298M
	[INFO] ------------------------------------------------------------------------
	[INFO] Scanning for projects...
	[INFO] 
	[INFO] ------------------------------------------------------------------------
	[INFO] Building facelog-db2-base 2.5.2-SNAPSHOT
	[INFO] ------------------------------------------------------------------------
	[INFO] 
	[INFO] --- maven-enforcer-plugin:1.0:enforce (enforce-maven) @ facelog-db2-base ---
	[INFO] 
	[INFO] --- buildnumber-maven-plugin:1.4:create (create-buildnumber) @ facelog-db2-base ---
	[INFO] Executing: cmd.exe /X /C "git rev-parse --verify HEAD"
	[INFO] Working directory: J:\facelog\db2\facelog-db-base
	[INFO] Storing buildNumber: 3ce5bd24faa4fa3a6727cae22a4dd4bbb6c20718 at timestamp: 1576333552755
	[INFO] Storing buildScmBranch: test
	[INFO] 
	[INFO] --- buildnumber-maven-plugin:1.4:create-timestamp (create-timestamp) @ facelog-db2-base ---
	[INFO] 
	[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ facelog-db2-base ---
	[INFO] Using 'UTF-8' encoding to copy filtered resources.
	[INFO] Copying 2 resources
	[INFO] 
	[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ facelog-db2-base ---
	[INFO] Changes detected - recompiling the module!
	[INFO] Compiling 34 source files to J:\facelog\db2\facelog-db-base\target\classes
	[INFO] 
	[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ facelog-db2-base ---
	[INFO] Using 'UTF-8' encoding to copy filtered resources.
	[INFO] skip non existing resourceDirectory J:\facelog\db2\facelog-db-base\src\test\resources
	[INFO] 
	[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ facelog-db2-base ---
	[INFO] Nothing to compile - all classes are up to date
	[INFO] 
	[INFO] --- maven-surefire-plugin:2.18.1:test (default-test) @ facelog-db2-base ---
	[INFO] Tests are skipped.
	[INFO] 
	[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ facelog-db2-base ---
	[INFO] Building jar: J:\facelog\db2\facelog-db-base\target\facelog-db2-base-2.5.2-SNAPSHOT.jar
	[INFO] 
	[INFO] --- maven-install-plugin:2.4:install (default-install) @ facelog-db2-base ---
	[INFO] Installing J:\facelog\db2\facelog-db-base\target\facelog-db2-base-2.5.2-SNAPSHOT.jar to J:\maven_repository\com\gitee\l0km\facelog-db2-base\2.5.2-SNAPSHOT\facelog-db2-base-2.5.2-SNAPSHOT.jar
	[INFO] Installing J:\facelog\db2\facelog-db-base\pom.xml to J:\maven_repository\com\gitee\l0km\facelog-db2-base\2.5.2-SNAPSHOT\facelog-db2-base-2.5.2-SNAPSHOT.pom
	[INFO] ------------------------------------------------------------------------
	[INFO] BUILD SUCCESS
	[INFO] ------------------------------------------------------------------------
	[INFO] Total time: 3.172 s
	[INFO] Finished at: 2019-12-14T22:25:54+08:00
	[INFO] Final Memory: 19M/308M
	[INFO] ------------------------------------------------------------------------


# docker

Dockerfile 用于创建一个基于mysql:5.6的facelog数据库的docker 镜像的脚本

